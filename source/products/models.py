from django.db import models

from organizations.models import Organization


class Category(models.Model):
    title = models.CharField(verbose_name='Наименование категории', max_length=128, unique=True)

    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'

    def __str__(self):
        return f'{self.title}'


class Product(models.Model):
    title = models.CharField(verbose_name='Наименование товара', max_length=128)
    category = models.ForeignKey(Category, related_name='products', verbose_name='Категория', on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Товар'
        verbose_name_plural = 'Товары'

    def __str__(self):
        return f'{self.title}'


class ProductPrice(models.Model):
    product = models.ForeignKey(Product, verbose_name='Товар', related_name='prices', on_delete=models.CASCADE)
    organization = models.ForeignKey(Organization, verbose_name='Предприятие',
                                     related_name='product_prices', on_delete=models.CASCADE)
    price = models.DecimalField(max_digits=9, decimal_places=2, verbose_name='Цена')

    class Meta:
        unique_together = ('product', 'organization')
        verbose_name = 'Цена товара'
        verbose_name_plural = 'Цены товаров'

    def __str__(self):
        return f'{self.organization.title}: {self.product.title} {self.price}'
