from django.conf.urls import re_path

from . import views

urlpatterns = [
    re_path(r'^about/(?P<pk>[0-9]+)/$', views.ProductPriceDetail.as_view(), name='product_price-detail'),
]
