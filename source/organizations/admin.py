from django.contrib import admin
from .models import OrganizationsNetwork, Organization, District


@admin.register(Organization)
class OrganizationAdmin(admin.ModelAdmin):
    pass


admin.site.register(OrganizationsNetwork)
admin.site.register(District)
