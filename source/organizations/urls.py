from django.conf.urls import re_path

from . import views

urlpatterns = [
    re_path(r'^about/(?P<pk>[0-9]+)/$', views.OrganizationDetail.as_view(), name='organization-detail'),
    re_path(r'^(?P<district_id>[0-9]+)/$', views.OrganizationByDistrictList.as_view(),
            name='organization-by-district-list'),
    re_path(r'^$', views.OrganizationList.as_view(), name='organization-list'),
]
